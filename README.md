# funary

![npm version](https://img.shields.io/npm/v/funary.svg) [![Build Status](https://travis-ci.org/EESimulations/funary.svg)](https://travis-ci.org/EESimulations/funary)
[![Dependency Status](https://david-dm.org/EESimulations/funary.svg)](https://david-dm.org/EESimulations/funary)

<img src="https://i.imgur.com/xedwvyA.png" width="200" height="200">

Arbitrary precision binary operations library for Javascript.



## Table of contents

* [Install](#install)
* [Logic gates](#logic-gates)
* [Bitwise operations & bit shifts](#bitwise-operations--bit-shifts)
* [Big endian operations](#big-endian-operations)
  * [Big endian arithmetic](#big-endian-arithmetic)
  * [Big endian encoders & decoders](#big-endian-encoders--decoders)
  * [Big endian multiplexers & demultiplexers](#big-endian-multiplexers--demultiplexers)
  * [Big endian conversions](#big-endian-conversions)
* [Little endian operations](#little-endian-operations)
  * [Little endian arithmetic](#little-endian-arithmetic)
  * [Little endian encoders & decoders](#little-endian-encoders--decoders)
  * [Little endian multiplexers & demultiplexers](#little-endian-multiplexers--demultiplexers)
  * [Little endian conversions](#little-endian-conversions)
* [Full & Half adders](#full--half-adders)
* [General conversions](#general-conversions)
* [Other operations](#other-operations)
* [Related projects](#related-projects)


## Install

[NPM](https://www.npmjs.com/package/funary)

```
npm install funary
```

```javascript
var funary = require('funary'); 
```

Clientside

```html
<script src="funary.js"></script>
```

## Logic gates

The library has XOR,AND,OR,NAND,XNOR, and NOR logic gates.

```javascript
var funary = require('funary'); 
funary.gates.and([1,1,1,1,1,0]) //0
funary.gates.and([1,1,1,1,1,1]) //1
funary.gates.xor([0,0,0,0,1,0]) //1
funary.gates.xor([1,0,0,0,0,1]) //0
funary.gates.or([0,0,0,0,0,1]) //1
funary.gates.or([0,0,0,0]) //0
```

## Bitwise operations & bit shifts

Bitwise operations and bit shifts for bit manipulation.

```javascript
var funary = require('funary');
//Bitwise operations. Similar selection to gates. AND,OR , ... etc.
funary.bitwise.and([0,1,0,1,0,1,0,1,1,1],[1,0,0,0,1,1,1,1,0,1]) //[0,0,0,0,0,1,0,1,0,1]
//   0101010111
//&  1000111101
//-------------
//   0000010101
funary.bitwise.or([0,1,0,1,0,1,0,1,1,1],[1,0,0,0,1,1,1,1,0,1]) //[1,1,0,1,1,1,1,1,1,1]
//   0101010111
//|  1000111101
//-------------
//   1101111111
funary.bitwise.xnor([0,1,0,1,0,1,0,1,1,1],[1,0,0,0,1,1,1,1,0,1]) //[0,0,1,0,0,1,0,1,0,1]
//     0101010111
//XNOR 1000111101
//---------------
//     0010010101

//Bit shifts
funary.left([0,0,1,0,1,1],2) //[1,0,1,1,0,0]
funary.right([0,0,1,0,1,1],2) //[0,0,0,0,1,0]
```

## Big endian operations

Operations done in Big endian. Most significant digit in smallest address

## Big endian arithmetic

Arithmetic operations. Any two arrays with any length.

```javascript
var funary = require('funary');
//Addition
funary.big.NADDER([0,1,0,1,0,1,0,1,1,1],[1,0,0,0,1,1,1,1,0,1]) //[1,1,1,0,0,1,0,1,0,0]
//  0101010111
//+ 1000111101
//------------
//  1110010100

//Multiplication
funary.big.NMULTIPLY([1,0,1,0,1,1,0,0,1],[1,1,1,0,0,1,1,1]) //[1,0,0,1,1,0,1,1,1,0,1,0,0,1,1,1,1]
//          101011001
//X          11100111
//-------------------
//  10011011101001111
```

## Big endian encoders & decoders

Encode and decode arrays of any size.

```javascript
var funary = require('funary');
//Encoders
funary.big.NENCODE([0, 0, 0, 0, 0, 0, 0, 1]) //[0, 0, 0]
funary.big.NENCODE([0, 0, 0, 0, 0, 1, 0, 0]) //[0, 1, 0]
funary.big.NENCODE([0, 0, 0, 0, 0, 0, 1, 0]) //[0, 0, 1]

funary.big.NENCODE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0]) //[0, 0, 1, 0]
funary.big.NENCODE([0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) //[1, 1, 1, 0]

//Decoders
funary.big.NDECODE([0,0,0]) //[0, 0, 0, 0, 0, 0, 0, 1]
funary.big.NDECODE([0,0,1]) //[0, 0, 0, 0, 0, 0, 1, 0]
funary.big.NDECODE([0,1,0]) //[0, 0, 0, 0, 0, 1, 0, 0]

funary.big.NDECODE([0,0,1,0]) //[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0]
funary.big.NDECODE([1,1,1,0]) //[0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
```

## Big endian multiplexers & demultiplexers


```javascript
var funary = require('funary');
//Multiplexing
//                   selectors inputs
funary.big.NMULTIPLEX([0,0],[0,0,0,0]) //0
funary.big.NMULTIPLEX([0,0],[0,0,0,1]) //1
funary.big.NMULTIPLEX([0,1],[0,0,0,0]) //0
funary.big.NMULTIPLEX([0,1],[0,0,1,0]) //1

funary.big.NMULTIPLEX([0,1,1],[0,0,0,0,0,0,0,0]) //0
funary.big.NMULTIPLEX([0,1,1],[0,0,0,0,1,0,0,0]) //1

//Demultiplexing
//                     selectors inputs
funary.big.NDEMULTIPLEX([0,0],0) //[0, 0, 0, 0]
funary.big.NDEMULTIPLEX([0,0],1) //[0, 0, 0, 1]
funary.big.NDEMULTIPLEX([0,1],0) //[0, 0, 0, 0]
funary.big.NDEMULTIPLEX([1,0],1) //[0, 1, 0, 0]

funary.big.NDEMULTIPLEX([0,0,1],1) //[0, 0, 0, 0, 0, 0, 1, 0]
funary.big.NDEMULTIPLEX([0,0,1],0) //[0, 0, 0, 0, 0, 0, 0, 0]
```

## Big endian conversions

Conversions between formats

```javascript
var funary = require('funary');
//Decimal to binary
funary.big.toUnsignedBinary(12) //[1, 1, 0, 0]
//Binary to decimal
funary.big.toUnsignedDecimal([1, 1, 0, 1]) //13
```

## Little endian operations

Operations done in Little endian. Most significant digit in largest address

## Little endian arithmetic

Arithmetic operations. Any two arrays with any length.

```javascript
var funary = require('funary');
//Addition
funary.little.NADDER([0,1,0,1,0,1,0,1,1,1],[1,0,0,0,1,1,1,1,0,1]) //[1,1,0,1,1,0,0,1,0,1,1]
//  0101010111
//+ 1000111101
//-------------
//  11011001011

//Multiplication
funary.little.NMULTIPLY([1,1,1,0,1,0,1,0,1],[1,0,1,1]) //[1,1,0,1,0,1,1,0,1,0,0,0,1]
//  111010101
//X 1011
//---------------
//  1101011010001
```

## Little endian encoders & decoders

Encode and decode arrays of any size.

```javascript
var funary = require('funary');
//Encoders
funary.little.NENCODE([1, 0, 0, 0, 0, 0, 0, 0]) //[0,0,0]
funary.little.NENCODE([0, 1, 0, 0, 0, 0, 0, 0]) //[1,0,0]
funary.little.NENCODE([0, 0, 1, 0, 0, 0, 0, 0]) //[0,1,0]

funary.little.NENCODE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]) //[0,1,0,1]
funary.little.NENCODE([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]) //[1,1,0,1]

//Decoders
funary.little.NDECODE([0,0,0]) //[1, 0, 0, 0, 0, 0, 0, 0]
funary.little.NDECODE([1,0,0]) //[0, 1, 0, 0, 0, 0, 0, 0]
funary.little.NDECODE([0,1,0]) //[0, 0, 1, 0, 0, 0, 0, 0]

funary.little.NDECODE([0,1,0,1]) //[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
funary.little.NDECODE([1,1,0,1]) //[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
```

## Little endian multiplexers & demultiplexers


```javascript
var funary = require('funary');
//Multiplexing
//                   selectors inputs
funary.little.NMULTIPLEX([0,0],[0,0,0,0]) //0
funary.little.NMULTIPLEX([0,0],[1,0,0,0]) //1
funary.little.NMULTIPLEX([0,1],[0,0,0,0]) //0
funary.little.NMULTIPLEX([0,1],[0,0,1,0]) //1

funary.little.NMULTIPLEX([0,1,1],[0,0,0,0,0,0,0,0]) //0
funary.little.NMULTIPLEX([0,1,1],[0,0,0,0,0,0,1,0]) //1

//Demultiplexing
//                     selectors inputs
funary.little.NDEMULTIPLEX([0,0],0) //[0, 0, 0, 0]
funary.little.NDEMULTIPLEX([0,0],1) //[1, 0, 0, 0]
funary.little.NDEMULTIPLEX([0,1],0) //[0, 0, 0, 0]
funary.little.NDEMULTIPLEX([0,1],1) //[0, 0, 1, 0]

funary.little.NDEMULTIPLEX([0,0,1],1) //[0, 0, 0, 0, 1, 0, 0, 0]
funary.little.NDEMULTIPLEX([0,0,1],0) //[0, 0, 0, 0, 0, 0, 0, 0]
```

## Little endian conversions

Conversions between formats

```javascript
var funary = require('funary'); 
//Decimal to binary
funary.little.toUnsignedBinary(12) //[0, 0, 1, 1]
//Binary to decimal
funary.little.toUnsignedDecimal([1, 1, 1, 0, 1]) //23
```

## Full & Half adders
```javascript
var funary = require('funary'); 
//Little endian by default
funary.FA([1,1,1]) //[1,1]
funary.FA([1,1,0]) //[0,1]
funary.FA([1,0,1]) //[0,1]
funary.FA([0,0,1]) //[1,0]
funary.HA([1,0]) //[1,0]
funary.HA([1,1]) //[0,1]
```

## General conversions

```javascript
var funary = require('funary');
//Array of bits to string
funary.arrayToString([1,1,0,1,0,1,1,1,1,0,0]) //"11010111100"
//String to array of bits
funary.stringToArray('001000001111') //[0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1]
//Convert bool array to binary
funary.boolArrayToBinary([false,false,true,true,false]) //[0,0,1,1,0]
//Convert binary array to bool
funary.binaryArrayToBool([0,1,0,1,0,0]) //[false,true,false,true,false,false]
```

## Other operations

```javascript
var funary = require('funary'); 
funary.not(1) //0
funary.not(0) //1
funary.invert([0,1,1,0,0,1,1,1]) //[1,0,0,1,1,0,0,0]
//Truncation
funary.righttrunc([1, 1, 1, 0, 1, 0, 1, 0],4) //[1, 1, 1, 0]
funary.lefttrunc([1, 1, 1, 0, 1, 0, 1, 0],5) //[0, 1, 0, 1, 0]
//Number of ones
funary.countones([1,0,1,0,1,1,1]) //5
//Combinations
funary.getCombinations(3) //[[1,1,1],[0,1,1],[1,0,1],[0,0,1],[1,1,0],[0,1,0],[1,0,0],[0,0,0]]
//Zeros
funary.zeros(4) //[0,0,0,0]
//Add zeros
funary.addzeros([1],3) //[1,0,0,0]
//UNshift zeros
funary.unshiftzeros([1],3) //[0,0,0,1]
```

## Related projects

[DLCS](https://github.com/EESimulations/DLCS) ![npm version](https://img.shields.io/npm/v/dlcs.svg)

Digital Logic Circuit Simulation library. Create and simulate high level integrated circuits. Depends on funary.
