var funary = (function() {
    var version = '1.0.0';

    var funobj = {
        //Not function
        not:function (input) {
            if (input === 0)
            {
                return 1;
            }
            return 0;
        },
        //Invert bits
        invert:function(inputs){
            return inputs.map(funobj.not);
        },
        //Half adder
        HA:function (inputs) {
            var S = funobj.gates.xor(inputs);
            var C = funobj.gates.and(inputs);
            return [S,C];
        },
        FA:function (inputs) {
            var S = funobj.gates.xor(inputs);
            var C = (((inputs[0] & inputs[1]) | (inputs[1] & inputs[2])) | (inputs[0] & inputs[2]));
            return [S,C];
        },
        //Number of ones
        countones: function (inputs) {
            return inputs.reduce(function(previousValue, currentValue, index, array) {
                return previousValue + currentValue;
            });
        },
        //Bitwise operations
        bitwise: {
            and:function (inputs0,inputs1) {
                var arr = [];
                inputs0.forEach(function(element, index, array) {
                    arr.push(inputs0[index] & inputs1[index]);
                });
                return arr;
            },
            or:function (inputs0,inputs1) {
                var arr = [];
                inputs0.forEach(function(element, index, array) {
                    arr.push(inputs0[index] | inputs1[index]);
                });
                return arr;
            },
            xor:function (inputs0,inputs1) {
                var arr = [];
                inputs0.forEach(function(element, index, array) {
                    arr.push(inputs0[index] ^ inputs1[index]);
                });
                return arr;
            },
            nand:function (inputs0,inputs1) {
                var arr = [];
                inputs0.forEach(function(element, index, array) {
                    arr.push(funobj.not( inputs0[index] & inputs1[index]) );
                });
                return arr;
            },
            nor:function (inputs0,inputs1) {
                var arr = [];
                inputs0.forEach(function(element, index, array) {
                    arr.push(funobj.not( inputs0[index] | inputs1[index]) );
                });
                return arr;
            },
            xnor:function (inputs0,inputs1) {
                var arr = [];
                inputs0.forEach(function(element, index, array) {
                    arr.push(funobj.not( inputs0[index] ^ inputs1[index]) );
                });
                return arr;
            }
        },
        //bit shift left
        left:function (arr,num) {
            var newarr = [];
            var last = arr.length-1;
            arr.forEach(function(element, index, array) {
                if ((index + num) > last)
                {
                    newarr.push(0);
                }
                else
                {
                    newarr.push(arr[(index + num)]);
                }
            });
            return newarr;
        },
        //bit shift right
        right:function (arr,num) {
            var newarr = [];
            var last = arr.length-1;
            for (var i=arr.length-1;i>=0;--i){
                if ((i - num) < 0)
                {
                    newarr.unshift(0);
                }
                else
                {
                    newarr.unshift(arr[(i - num)]);
                }
            }
            return newarr;
        },
        arrayToString:function (arr) {
            var str = "";
            arr.forEach(function(element, index, array) {
                str += element.toString();
            });
            return str;
        },
        stringToArray:function (str) {
            var arr = [];
            for(var i =0;i<str.length;++i) {
                arr.push(parseInt(str[i],10));
            }
            return arr;
        },
        //Truncate from right
        righttrunc:function (arr,num) {
            return arr.slice(0,num);
        },
        //Truncate from left
        lefttrunc:function (arr,num) {
            return arr.slice(arr.length-num,arr.length);
        },
        //Generate binary combinations
        getCombinations:function(n) {
            var arr = [];
            for(var i = 0; i < (1 << n); i++) {
                var c = [];
                for(var j = 0; j < n; j++) {
                  c.push( !(i & (1 << j)) & 1 );
                }
                arr.push(c);
            }
            return arr;
        },
        //Add zeros to front
        unshiftzeros:function(arr,n) {
            for (var i = 0;i<n;++i)
            {
                arr.unshift(0);
            }
            return arr;
        },
        //Add zeros to back
        addzeros:function(arr,n) {
            for (var i = 0;i<n;++i)
            {
                arr.push(0);
            }
            return arr;
        },
        //Generate zeros
        zeros:function(n) {
            var arr = [];
            return funobj.addzeros(arr,n);
        },
        //Convert bool to binary
        boolToBinary:function(bool) {
            return bool | 0;
        },
        //Convert bool to binary
        binaryToBool:function(binary) {
            if (binary === 0)
            {
                return false;
            }
            return true;
        },
        //Convert bool array to binary
        boolArrayToBinary:function(arr) {
            return arr.map(funobj.boolToBinary);
        },
        //Convert binary array to bool
        binaryArrayToBool:function(arr) {
            return arr.map(funobj.binaryToBool);
        },
        log2:function(n) {
            var i = 0;
            while (n !== 1)
            {
                n >>= 1;
                ++i;
            }
            return i;
        },
        //Big endian operations
        big:{
            //Decoder
            NDECODE:function(arr) {
                var length = arr.length;
                var pos = funobj.big.toUnsignedDecimal(arr);
                var newlength = (1 << length);
                var newarr = funobj.zeros(newlength);
                newarr[(newlength-pos)-1] = 1;
                return newarr;
            },
            //Emcoder
            NENCODE:function(arr) {
                var length = arr.length;
                for(var i = 0;i<length;++i)
                {
                    if (arr[i] === 1)
                    {
                        break;
                    }
                }
                var newarr = funobj.big.toUnsignedBinary(length-i-1);
                var newlength = funobj.log2(length);
                funobj.unshiftzeros(newarr,newlength-newarr.length);
                return newarr;
            },
            //Multiply any two arbitrary sized bits
            NMULTIPLY:function(a,b) {
                //Give b the smallest array because shifting is expensive
                var na,nb;
                if (b.length > a.length)
                {
                    nb = a;
                    na = b;
                }
                else
                {
                    na = a;
                    nb = b;
                }

                //Last element
                var maxb = nb.length-1;
                var maxa = na.length-1;

                var arr = [];
                //First row
                if (nb[maxb] === 0)
                {
                    arr = funobj.zeros(na.length);
                }
                else
                {
                    arr = na.slice(0);
                }

                //Multiply the rest
                for (var i = maxb-1;i >= 0;--i)
                {
                    var temparr = [];

                    if (nb[i] === 0)
                    {
                         continue;
                    }
                    else
                    {
                        temparr = na.slice(0);
                    }

                    funobj.addzeros(temparr,maxb-i);
                    arr = funobj.big.NADDER(temparr,arr);
                }

                return arr;
            },
            toUnsignedBinary:function (integer) {
                var arr = [];
                while(integer > 0)
                {
                    arr.unshift(integer & 1);
                    integer = integer >> 1;
                }
                if (arr.length === 0)
                {
                    return [0];
                }

                return arr;
            },
            toUnsignedDecimal:function (arr){
                var length = arr.length-1;
                var sum = 0;
                arr.forEach(function(element, index, array) {
                    sum += ((element) << (length-index));
                });
                return sum;
            },
            //Multiplexer
            NMULTIPLEX: function(a0,a1) {
                var pos = funobj.big.toUnsignedDecimal(a0);
                if (pos < a1.length)
                {
                    return a1[a1.length-pos-1];
                }
                return 0;
            },
            //Demultiplexer
            NDEMULTIPLEX: function(a0,a1) {
                var arr = funobj.zeros( 1 << a0.length );
                var pos = funobj.big.toUnsignedDecimal(a0);
                arr[arr.length-pos-1] = a1;
                return arr;
            },
            //Add any two arbitrary sized bits
            NADDER:function (a0,a1) {
                if (a1.length >a0.length)
                {
                    funobj.unshiftzeros(a0,(a1.length-a0.length));
                }
                else if (a1.length < a0.length)
                {
                    funobj.unshiftzeros(a1,(a0.length-a1.length));
                }

                var carry = 0;
                var arr = [];
                for (var i=a0.length-1;i>=0;--i)
                {
                    var data = funobj.FA( [a0[i],a1[i],carry] );
                    arr.unshift(data[0]);
                    carry = data[1];
                }

                if (carry === 1)
                {
                    arr.unshift(carry);
                }

                return arr;
            }
        },
        //Little endian operations
        little:{
            //Decoder
            NDECODE:function(arr) {
                var length = arr.length;
                var pos = funobj.little.toUnsignedDecimal(arr);
                var newarr = funobj.zeros(1 << length);
                newarr[pos] = 1;
                return newarr;
            },
            //Emcoder
            NENCODE:function(arr) {
                var length = arr.length;
                for(var i = 0;i<length;++i)
                {
                    if (arr[i] === 1)
                    {
                        break;
                    }
                }
                var newarr = funobj.little.toUnsignedBinary(i);
                var newlength = funobj.log2(length);
                funobj.addzeros(newarr,newlength-newarr.length);
                return newarr;
            },
            //Multiply any two arbitrary sized bits
            NMULTIPLY:function(a,b) {
                //Give b the smallest array because shifting is expensive
                var na,nb;
                if (b.length > a.length)
                {
                    nb = a;
                    na = b;
                }
                else
                {
                    na = a;
                    nb = b;
                }

                var arr = [];
                //First row
                if (nb[0] === 0)
                {
                    arr = funobj.zeros(na.length);
                }
                else
                {
                    arr = na.slice(0);
                }

                //Multiply the rest
                for (var i = 1;i < nb.length;++i)
                {
                    var temparr = [];

                    if (nb[i] === 0)
                    {
                         continue;
                    }
                    else
                    {
                        temparr = na.slice(0);
                    }

                    funobj.unshiftzeros(temparr,i);
                    arr = funobj.little.NADDER(temparr,arr);
                }

                return arr;
            },
            toUnsignedBinary:function (integer) {
                var arr = [];
                while(integer > 0)
                {
                    arr.push(integer & 1);
                    integer = integer >> 1;
                }
                if (arr.length === 0)
                {
                    return [0];
                }

                return arr;
            },
            toUnsignedDecimal:function (arr){
                var sum = 0;
                arr.forEach(function(element, index, array) {
                    sum += (element << (index));
                });
                return sum;
            },
            //Multiplexer
            NMULTIPLEX: function(a0,a1) {
                var pos = funobj.little.toUnsignedDecimal(a0);
                if (pos < a1.length)
                {
                    return a1[pos];
                }
                return 0;
            },
            //Demultiplexer
            NDEMULTIPLEX: function(a0,a1) {
                var arr = funobj.zeros( 1 << a0.length );
                var pos = funobj.little.toUnsignedDecimal(a0);
                arr[pos] = a1;
                return arr;
            },
            //Add any two arbitrary sized bits
            NADDER:function (a0,a1) {

                if (a1.length >a0.length)
                {
                    funobj.addzeros(a0,(a1.length-a0.length));
                }
                else if (a1.length < a0.length)
                {
                    funobj.addzeros(a1,(a0.length-a1.length));
                }


                var carry = 0;
                var arr = [];
                for (var i=0;i<a0.length;++i)
                {
                    var data = funobj.FA( [a0[i],a1[i],carry] );
                    arr.push(data[0]);
                    carry = data[1];
                }

                if (carry === 1)
                {
                    arr.push(carry);
                }
                return arr;
            }
        },
        gates:{
            //Logic gates
            and:function (inputs) {
                return funobj.not(inputs.some(function (element, index, array) {
                    return element === 0;
                }) | 0);
            },
            or:function (inputs) {
                return (inputs.some(function (element, index, array) {
                    return element === 1;
                }) | 0);
            },
            xor:function (inputs) {
                return inputs.reduce(function(previousValue, currentValue, index, array) {
                    return previousValue ^ currentValue;
                });
            },
            nand:function (inputs) {
                return funobj.not(funobj.gates.and(inputs));
            },
            nor:function (inputs) {
                return funobj.not(funobj.gates.or(inputs));
            },
            xnor:function (inputs) {
                return funobj.not(funobj.gates.xor(inputs));
            }
        },
        //Interface to DLCS
        DLCS:{
            not:function (input) {
                if (input[0] === 0)
                {
                    return [1];
                }
                return [0];
            },
            and:function (input) {
                return [funobj.gates.and(input)];
            },
            or:function (input) {
                return [funobj.gates.or(input)];
            },
            xor:function (input) {
                return [funobj.gates.xor(input)];
            },
            nand:function (input) {
                return [funobj.gates.nand(input)];
            },
            nor:function (input) {
                return [funobj.gates.nor(input)];
            },
            xnor:function (input) {
                return [funobj.gates.xnor(input)];
            },
            ha:function (input) {
                return funobj.HA(input);
            },
            fa:function (input) {
                return funobj.FA(input);
            },
            nadder:function (input) {
                var half = input.length/2;
                return funobj.little.NADDER(input.slice(0).splice(0,half),input.slice(0).splice(half));
            },
            nmultiply:function (input) {
                var half = input.length/2;
                return funobj.little.NMULTIPLY(input.slice(0).splice(0,half),input.slice(0).splice(half));
            },
            ndecoder:function (input) {
                return funobj.little.NDECODE(input);
            },
            nencode:function (input) {
                return funobj.little.NENCODE(input);
            },
            nmultiplex:function (input) {
                return funobj.little.NMULTIPLEX(input);
            },
            ndemultiplex:function (input) {
                return funobj.little.NDEMULTIPLEX(input);
            }
        }

    };


    return funobj;
})();





if((typeof module) !== 'undefined') {
    module.exports = funary;
}
