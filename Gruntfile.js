module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({

        clean: ["docs", "build"],
        copy: {
            dist: {
                files: [
                    { expand: true, src: ['LICENSE'], dest: 'build/' },
                    { expand: true, src: ['**.json'], dest: 'build/' },
                    { expand: true, src: ['**.md'], dest: 'build/' }
                ]
            }
        },

        uglify: {
            my_target: {
                files: {
                    'build/funary.js': 'funary.js'
                }
            }
        },
        qunit: {
            files: ['tests/test.html']
        }
    });

    // Load plugins
    grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    // Task to run tests
    grunt.registerTask('test', 'qunit');
    grunt.registerTask('clear', 'clean');
    grunt.registerTask('build', [ 'qunit', 'clean','uglify','copy'  ]);
};
