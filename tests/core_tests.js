QUnit.module( "funary.js" );

QUnit.test( "Gate function test", function( assert ) {

    var test_cases = [
        {
            description: "AND gate",
            gate:"and",
            inputs: [[0,0],[1,0],[0,1],[1,1],[1,1,1,0],[1,0,1,1,1],[1,1,1,1,1]],
            expected: [0,0,0,1,0,0,1]
        },
        {
            description: "OR gate",
            gate:"or",
            inputs: [[0,0],[1,0],[0,1],[1,1],[1,1,1,0],[1,0,1,1,1],[1,1,1,1,1],[0,0,0,0,0]],
            expected: [0,1,1,1,1,1,1,0]
        },
        {
            description: "XOR gate",
            gate:"xor",
            inputs: [[0,0],[1,0],[0,1],[1,1],[1,1,1,0],[1,0,1,1,1],[1,1,1,1,1],[0,0,0,0,0]],
            expected: [0,1,1,0,1,0,1,0]
        },
        {
            description: "NAND gate",
            gate:"nand",
            inputs: [[0,0],[1,0],[0,1],[1,1],[1,1,1,0],[1,0,1,1,1],[1,1,1,1,1]],
            expected: [1,1,1,0,1,1,0]
        },
        {
            description: "NOR gate",
            gate:"nor",
            inputs: [[0,0],[1,0],[0,1],[1,1],[1,1,1,0],[1,0,1,1,1],[1,1,1,1,1],[0,0,0,0,0]],
            expected: [1,0,0,0,0,0,0,1]
        },
        {
            description: "XNOR gate",
            gate:"xnor",
            inputs: [[0,0],[1,0],[0,1],[1,1],[1,1,1,0],[1,0,1,1,1],[1,1,1,1,1],[0,0,0,0,0]],
            expected: [1,0,0,1,0,1,0,1]
        }
    ];
    var run_tests = function() {
        test_cases.forEach(function(element, index, array) {
            var test_case = element; //get the test case
            var result = [];

            result  = test_case.inputs.map(funary.gates[test_case.gate]);

            assert.deepEqual( result, test_case.expected, test_case.description );
        });
    };
    run_tests();
});

QUnit.test( "NADDER test", function( assert ) {
    var test_cases = [
        {
            description: "Little NADDER",
            place:"little",
            func:"NADDER",
            a0:       [[0],  [1],  [0],  [1],   [0,0,1,1],   [0,1,0,1,0,1,0,1,1,1],   [0,1],     [1,1,1],   [1,1],       [1,1,0,1,1] ],
            a1:       [[0],  [0],  [1],  [1],   [0,1,1,1],   [1,0,0,0,1,1,1,1,0,1],   [1,1,1],   [0,1],     [1,1,0,1,1], [1,1]       ],
            expected: [[0],  [1],  [1],  [0,1], [0,1,0,1,1], [1,1,0,1,1,0,0,1,0,1,1], [1,0,0,1], [1,0,0,1], [0,1,1,1,1], [0,1,1,1,1] ]
        },
        {
            description: "Big NADDER",
            place:"big",
            func:"NADDER",
            a0:       [[0],  [1],  [0],    [1],   [0,1,1],   [0,1,0,1,0,1,0,1,1,1],      [1,0],   [1,1,1],        [1,1], [1,1,0,1,1] ],
            a1:       [[0],  [0],  [1],    [1],   [1,1,1],   [1,0,0,0,1,1,1,1,0,1],    [1,1,1],     [1,0],  [1,1,0,1,1],       [1,1] ],
            expected: [[0],  [1],  [1],  [1,0], [1,0,1,0],   [1,1,1,0,0,1,0,1,0,0],  [1,0,0,1], [1,0,0,1],  [1,1,1,1,0], [1,1,1,1,0] ]
        }
    ];
    var run_tests = function() {
        test_cases.forEach(function(element, index, array) {
            var test_case = element; //get the test case
            var result = [];
            test_case.a0.forEach( function(element, index, array) {
                result.push(funary[test_case.place][test_case.func](test_case.a0[index],test_case.a1[index]) );
            });

            assert.deepEqual( result, test_case.expected, test_case.description );
        });
    };
    run_tests();
});

QUnit.test( "Bitwise test", function( assert ) {

    var test_cases = [
        {
            description: "bitwise AND",
            func:"and",
            a0: [[0],[1],[0],[1],[0,0,1,1],[0,1,0,1,0,1,0,1,1,1]],
            a1: [[0],[0],[1],[1],[0,1,1,1],[1,0,0,0,1,1,1,1,0,1]],
            expected: [[0],[0],[0],[1],[0,0,1,1],[0,0,0,0,0,1,0,1,0,1]]
        },
        {
            description: "bitwise OR",
            func:"or",
            a0: [[0],[1],[0],[1],[0,0,1,1],[0,1,0,1,0,1,0,1,1,1]],
            a1: [[0],[0],[1],[1],[0,1,1,1],[1,0,0,0,1,1,1,1,0,1]],
            expected: [[0],[1],[1],[1],[0,1,1,1],[1,1,0,1,1,1,1,1,1,1]]
        },
        {
            description: "bitwise XOR",
            func:"xor",
            a0: [[0],[1],[0],[1],[0,0,1,1],[0,1,0,1,0,1,0,1,1,1]],
            a1: [[0],[0],[1],[1],[0,1,1,1],[1,0,0,0,1,1,1,1,0,1]],
            expected: [[0],[1],[1],[0],[0,1,0,0],[1,1,0,1,1,0,1,0,1,0]]
        },
        {
            description: "bitwise NAND",
            func:"nand",
            a0: [[0],[1],[0],[1],[0,0,1,1],[0,1,0,1,0,1,0,1,1,1]],
            a1: [[0],[0],[1],[1],[0,1,1,1],[1,0,0,0,1,1,1,1,0,1]],
            expected: [[1],[1],[1],[0],[1,1,0,0],[1,1,1,1,1,0,1,0,1,0]]
        },
        {
            description: "bitwise NOR",
            func:"nor",
            a0: [[0],[1],[0],[1],[0,0,1,1],[0,1,0,1,0,1,0,1,1,1]],
            a1: [[0],[0],[1],[1],[0,1,1,1],[1,0,0,0,1,1,1,1,0,1]],
            expected: [[1],[0],[0],[0],[1,0,0,0],[0,0,1,0,0,0,0,0,0,0]]
        },
        {
            description: "bitwise XNOR",
            func:"xnor",
            a0: [[0],[1],[0],[1],[0,0,1,1],[0,1,0,1,0,1,0,1,1,1]],
            a1: [[0],[0],[1],[1],[0,1,1,1],[1,0,0,0,1,1,1,1,0,1]],
            expected: [[1],[0],[0],[1],[1,0,1,1],[0,0,1,0,0,1,0,1,0,1]]
        }

    ];
    var run_tests = function() {
        test_cases.forEach(function(element, index, array) {
            var test_case = element; //get the test case
            var result = [];

            test_case.a0.forEach( function(element, index, array) {
                result.push(funary.bitwise[test_case.func](test_case.a0[index],test_case.a1[index]) );
            });

            assert.deepEqual( result, test_case.expected, test_case.description );
        });
    };
    run_tests();
});

QUnit.test( "Shift test", function( assert ) {

    var test_cases = [
        {
            description: "shift left",
            func:"left",
            a0: [[0,0,1,0,1,1],[0,0,1,0,1,1],[0,0,1,0,1,1],[0,0,1,0,1,1],[0,0,1,0,1,1]],
            a1: [0,1,2,3,4],
            expected: [[0,0,1,0,1,1],[0,1,0,1,1,0],[1,0,1,1,0,0],[0,1,1,0,0,0],[1,1,0,0,0,0]]
        },
        {
            description: "shift right",
            func:"right",
            a0: [[0,0,1,0,1,1],[0,0,1,0,1,1],[0,0,1,0,1,1],[0,0,1,0,1,1],[0,0,1,0,1,1]],
            a1: [0,1,2,3,4],
            expected: [[0,0,1,0,1,1],[0,0,0,1,0,1],[0,0,0,0,1,0],[0,0,0,0,0,1],[0,0,0,0,0,0]]
        }

    ];
    var run_tests = function() {
        test_cases.forEach(function(element, index, array) {
            var test_case = element; //get the test case
            var result = [];

            test_case.a0.forEach( function(element, index, array) {
                result.push(funary[test_case.func](test_case.a0[index],test_case.a1[index]) );
            });

            assert.deepEqual( result, test_case.expected, test_case.description );
        });
    };
    run_tests();
});

QUnit.test( "Multiply test", function( assert ) {
    var test_cases = [
        {
            description: "Little NMULTIPLY",
            place:"little",
            func:"NMULTIPLY",
            a0:       [[0],[1],[0],[1],[1],      [0,1],        [1,1,1],            [1,1,1,0,1,0,1,0,1],        [1,1,1,1,1]],
            a1:       [[0],[0],[1],[1],[0,1,1,1],[0,1,1,1],    [0,1,1,0,1,1],      [1,0,1,1],                  [1,1,1,1,1]],
            expected: [[0],[0],[0],[1],[0,1,1,1],[0,0,1,1,1],  [0,1,0,1,1,1,1,0,1],[1,1,0,1,0,1,1,0,1,0,0,0,1],[1,0,0,0,0,0,1,1,1,1]]
        },
        {
            description: "Big NMULTIPLY",
            place:"big",
            func:"NMULTIPLY",
            a0:       [[0],[1],[0],[1],[1],      [1,0],          [1,0,1,0],    [1,0,1,0,1,1,0,0,1]],
            a1:       [[0],[0],[1],[1],[1,1,0,1],[1,1,1,0,1],    [1,1],        [1,1,1,0,0,1,1,1]],
            expected: [[0],[0],[0],[1],[1,1,0,1],[1,1,1,0,1,0],  [1,1,1,1,0],  [1,0,0,1,1,0,1,1,1,0,1,0,0,1,1,1,1]]
        }
    ];
    var run_tests = function() {
        test_cases.forEach(function(element, index, array) {
            var test_case = element; //get the test case
            var result = [];
            test_case.a0.forEach( function(element, index, array) {
                result.push(funary[test_case.place][test_case.func](test_case.a0[index],test_case.a1[index]) );
            });

            assert.deepEqual( result, test_case.expected, test_case.description );
        });
    };
    run_tests();
});



QUnit.test( "Encoders & decoders test", function( assert ) {

    var test_cases = [
        {
            description: "little decoder",
            place:"little",
            func:"NDECODE",
            a:        [[0,0],    [1,0],    [1,1],    [0,0,0],          [1,0,0],          [1,0,1],          [1,1,1,0],                        [1,0,1,1]],
            expected: [[1,0,0,0],[0,1,0,0],[0,0,0,1],[1,0,0,0,0,0,0,0],[0,1,0,0,0,0,0,0],[0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0]]
        },
        {
            description: "little encoder",
            place:"little",
            func:"NENCODE",
            a:       [[1,0,0,0],[0,1,0,0],[0,0,0,1],[1,0,0,0,0,0,0,0],[0,1,0,0,0,0,0,0],[0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0]],
            expected:[[0,0],    [1,0],    [1,1],    [0,0,0],          [1,0,0],          [1,0,1],          [1,1,1,0],                        [1,0,1,1]],
        },
        {
            description: "big decoder",
            place:"big",
            func:"NDECODE",
            a:        [[0,0],    [0,1],    [1,1],    [0,0,0],          [0,0,1],          [1,0,1],          [1,1,1,0],                        [1,0,1,1]],
            expected: [[0,0,0,1],[0,0,1,0],[1,0,0,0],[0,0,0,0,0,0,0,1],[0,0,0,0,0,0,1,0],[0,0,1,0,0,0,0,0],[0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0]]
        },
        {
            description: "big encoder",
            place:"big",
            func:"NENCODE",
            a:       [[0,0,0,1],[0,0,1,0],[1,0,0,0],[0,0,0,0,0,0,0,1],[0,0,0,0,0,0,1,0],[0,0,1,0,0,0,0,0],[0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0]],
            expected:[[0,0],    [0,1],    [1,1],    [0,0,0],          [0,0,1],          [1,0,1],          [1,1,1,0],                        [1,0,1,1]],
        }
    ];
    var run_tests = function() {
        test_cases.forEach(function(element, index, array) {
            var test_case = element; //get the test case
            var result = [];

            test_case.a.forEach( function(element, index, array) {
                result.push(funary[test_case.place][test_case.func](test_case.a[index]) );
            });

            assert.deepEqual( result, test_case.expected, test_case.description );
        });
    };
    run_tests();
});


QUnit.test( "Multiplex & demultiplex test", function( assert ) {

    var test_cases = [
        {
            description: "little multiplexer",
            place:"little",
            func:"NMULTIPLEX",
            a0:        [[0,0],     [0,0],     [1,0],     [1,0],     [0,1],     [0,1],     [1,1],     [1,1],     [0,1,1],           [0,1,1],           [1,0,0],           [1,0,0]           ],
            a1:        [[0,0,0,0], [1,0,0,0], [0,0,0,0], [0,1,0,0], [0,0,0,0], [0,0,1,0], [0,0,0,0], [0,0,0,1], [0,0,0,0,0,0,0,0], [0,0,0,0,0,0,1,0], [0,0,0,0,0,0,0,0], [0,1,0,0,0,0,0,0] ],
            expected:  [0,         1,         0,         1,         0,         1,         0,         1,         0,                 1,                 0,                 1                 ]
        },
        {
            description: "big multiplexer",
            place:"big",
            func:"NMULTIPLEX",
            a0:        [[0,0],     [0,0],     [1,0],     [1,0],     [0,1],     [0,1],     [1,1],     [1,1],     [0,1,1],           [0,1,1],           [1,0,0,0],           [1,0,0,0]           ],
            a1:        [[0,0,0,0], [0,0,0,1], [0,0,0,0], [0,1,0,0], [0,0,0,0], [0,0,1,0], [0,0,0,0], [1,0,0,0], [0,0,0,0,0,0,0,0], [0,0,0,0,1,0,0,0], [0,0,0,0,0,0,0,0,0], [1,0,0,0,0,0,0,0,0] ],
            expected:  [0,         1,         0,         1,         0,         1,         0,         1,         0,                 1,                 0,                   1                   ]
        },
        {
            description: "little demultiplexer",
            place:"little",
            func:"NDEMULTIPLEX",
            a0:        [[0,0],     [0,0],     [1,0],     [1,0],     [0,1],     [0,1],     [1,1],     [1,1],     [0,1,1],           [0,1,1],           [1,0,0],           [1,0,0]           ],
            a1:        [0,         1,         0,         1,         0,         1,         0,         1,         0,                 1,                 0,                 1                 ],
            expected:  [[0,0,0,0], [1,0,0,0], [0,0,0,0], [0,1,0,0], [0,0,0,0], [0,0,1,0], [0,0,0,0], [0,0,0,1], [0,0,0,0,0,0,0,0], [0,0,0,0,0,0,1,0], [0,0,0,0,0,0,0,0], [0,1,0,0,0,0,0,0] ]
        },
        {
            description: "big demultiplexer",
            place:"big",
            func:"NDEMULTIPLEX",
            a0:        [[0,0],     [0,0],     [1,0],     [1,0],     [0,1],     [0,1],     [1,1],     [1,1],     [0,1,1],           [1,0,1],           [1,0,0],           [1,0,0]           ],
            a1:        [0,         1,         0,         1,         0,         1,         0,         1,         0,                 1,                 0,                 1                 ],
            expected:  [[0,0,0,0], [0,0,0,1], [0,0,0,0], [0,1,0,0], [0,0,0,0], [0,0,1,0], [0,0,0,0], [1,0,0,0], [0,0,0,0,0,0,0,0], [0,0,1,0,0,0,0,0], [0,0,0,0,0,0,0,0], [0,0,0,1,0,0,0,0] ]
        }
    ];
    var run_tests = function() {
        test_cases.forEach(function(element, index, array) {
            var test_case = element; //get the test case
            var result = [];

            test_case.a0.forEach( function(element, index, array) {
                result.push(funary[test_case.place][test_case.func]( test_case.a0[index],test_case.a1[index] ) );
            });

            assert.deepEqual( result, test_case.expected, test_case.description );
        });
    };
    run_tests();
});




QUnit.test( "Full & half adder test", function( assert ) {

    var test_cases = [
        {
            description: "Half adder",
            func:"HA",
            a: funary.getCombinations(2),
            expected: [[0,1],[1,0],[1,0],[0,0]]
        },
        {
            description: "Full adder",
            func:"FA",
            a: funary.getCombinations(3),
            expected: [[1,1],[0,1],[0,1],[1,0],[0,1],[1,0],[1,0],[0,0]]
        }
    ];
    var run_tests = function() {
        test_cases.forEach(function(element, index, array) {
            var test_case = element; //get the test case
            var result = [];

            test_case.a.forEach( function(element, index, array) {
                result.push(funary[test_case.func](test_case.a[index]) );
            });

            assert.deepEqual( result, test_case.expected, test_case.description );
        });
    };
    run_tests();
});


QUnit.test( "Other operations test", function( assert ) {
    assert.deepEqual( funary.big.toUnsignedBinary(12) , [1, 1, 0, 0], "Big toUnsignedBinary" );
    assert.deepEqual( funary.big.toUnsignedBinary(20) , [1, 0, 1, 0, 0], "Big toUnsignedBinary 2" );
    assert.deepEqual( funary.big.toUnsignedBinary(10) , [1, 0, 1, 0], "Big toUnsignedBinary 3" );

    assert.deepEqual( funary.little.toUnsignedBinary(12) , [0, 0, 1, 1], "Little toUnsignedBinary" );
    assert.deepEqual( funary.little.toUnsignedBinary(20) , [0, 0, 1, 0, 1], "Little toUnsignedBinary 2" );
    assert.deepEqual( funary.little.toUnsignedBinary(10) , [0, 1, 0, 1], "Little toUnsignedBinary 3" );

    assert.deepEqual( funary.big.toUnsignedDecimal([1, 1, 0, 0]) , 12, "Big toUnsignedDecimal" );
    assert.deepEqual( funary.big.toUnsignedDecimal([1, 0, 1, 0, 0]) , 20, "Big toUnsignedDecimal 2" );
    assert.deepEqual( funary.big.toUnsignedDecimal([1, 0, 1, 0]) , 10, "Big toUnsignedDecimal 3" );
    assert.deepEqual( funary.big.toUnsignedDecimal([1, 1, 0, 1]) , 13, "Big toUnsignedDecimal 4" );

    assert.deepEqual( funary.little.toUnsignedDecimal([0, 0, 1, 1]) , 12, "Little toUnsignedDecimal" );
    assert.deepEqual( funary.little.toUnsignedDecimal([0, 0, 1, 0, 1]) , 20, "Little toUnsignedDecimal 2" );
    assert.deepEqual( funary.little.toUnsignedDecimal([0, 1, 0, 1]) , 10, "Little toUnsignedDecimal 3" );
    assert.deepEqual( funary.little.toUnsignedDecimal([1, 0, 1, 1]) , 13, "Little toUnsignedDecimal 4" );

    assert.deepEqual( funary.invert([0,1,1,0,0,1]), [1,0,0,1,1,0], "Invert" );
    assert.deepEqual( funary.invert([1,0,0,0,1,0,0,0,1,1,1,1]), [0,1,1,1,0,1,1,1,0,0,0,0] , "Invert 2" );
    assert.deepEqual( funary.not(1), 0, "NOT" );
    assert.deepEqual( funary.not(0), 1, "NOT 2" );

    assert.deepEqual( funary.righttrunc([1, 1, 1, 0, 1, 0, 1, 0],4) ,[1, 1, 1, 0], "Right truncation" );
    assert.deepEqual( funary.righttrunc([1, 1, 1, 0, 1, 0, 1, 0],5) ,[1, 1, 1, 0, 1], "Right truncation 2" );
    assert.deepEqual( funary.lefttrunc([1, 1, 1, 0, 1, 0, 1, 0],5) ,[0, 1, 0, 1, 0], "Left truncation" );
    assert.deepEqual( funary.lefttrunc([1, 1, 1, 0, 1, 0, 1, 0],7) ,[1, 1, 0, 1, 0, 1, 0], "Left truncation 2" );

    assert.deepEqual( funary.arrayToString([1,1,0,1,0,1,1,1,1,0,0]) ,"11010111100", "Array to string" );
    assert.deepEqual( funary.stringToArray('001000001111') ,[0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1], "String to array" );

    assert.deepEqual( funary.getCombinations(2) , [[1,1],[0,1],[1,0],[0,0]] , "Combinations" );
    assert.deepEqual( funary.getCombinations(3) , [[1,1,1],[0,1,1],[1,0,1],[0,0,1],[1,1,0],[0,1,0],[1,0,0],[0,0,0]] , "Combinations 2" );

    assert.deepEqual( funary.zeros(4) , [0,0,0,0] , "Zeros" );
    assert.deepEqual( funary.zeros(5) , [0,0,0,0,0] , "Zeros 2" );

    assert.deepEqual( funary.boolArrayToBinary([false,false,true,true,false]) , [0,0,1,1,0] , "boolArrayToBinary" );
    assert.deepEqual( funary.binaryArrayToBool([0,1,0,1,0,0]) , [false,true,false,true,false,false], "binaryArrayToBool" );
});

